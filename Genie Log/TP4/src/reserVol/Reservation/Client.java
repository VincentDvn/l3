package reserVol.Reservation;

import java.util.ArrayList;
import reserVol.GestionVol.Vol;

public class Client extends Personne
{

    private ArrayList<Reservation> listReserv = new ArrayList<>();
    private String nom;
    private String email;
    private String telephone; 
    private String paiement;                   //Correspond à ses coordonnées bancaires
    
    public Client(String name, String mail, String tel, String paye)
    {
        super(name, mail, tel);
        this.paiement = paye;
    }
/*          Cette fonction va permettre d'ajouter une nouvelle réservation          */
    public void reserver(Vol volAReserver, Passager passanger)
    {
        listReserv.add(new Reservation(volAReserver, passanger, this));
    }
/*          Cette fonction va permettre d'envoyer un message dans le cas d'une      */
/*          annualtion de réservation ou de vol.                                    */
    protected void contacter(Reservation volAnnuler)
    {
        if(listReserv.remove(volAnnuler))
        {
            /*
            Envoie un message sur la boite Mail du client (de l'objet) pour indiquer l'annulation du vol réserver
            */
        }
        else 
        {
            throw new IllegalArgumentException("Le client n'a pas réserver ce vol");
        }
    }

    
    /*          Cette fonction va être appelé lorsqu'une réservation pas encore payée est confirmé  */
    private void payer()
    {
        //Paiement grâce aux coordonnées bancaires
    }


    /*          Cette fonction va être appelé lorsqu'une réservation déjà payée est annulée         */
    private void rembourser()
    {
        //Rembourse le client grâce aux coordonnées bancaires
    }


    /*      -----------------------------------------------------------------------------------      */
    /*          Cette fonction va permettre de vérifier les conditions pour faire payer un client    */
    /*          (qu'il ai déjà ajouté (EN_ATTENTE) une réservation et qu'il ne l'ai pas confirmée    */
    /*          ni annulée).                                                                         */
    /*      -----------------------------------------------------------------------------------      */
    public void clientConfirmer(Reservation reserv)
    {
        if(listReserv.contains(reserv))
        {
            if(reserv.reservConfirmer())
            {
                this.payer();
            }            
        }
        else
        {
            throw new IllegalArgumentException("La réservation que vous avez voulu confirmer n'est pas reconnu");
        }
    }

    /*      -----------------------------------------------------------------------------------      */
    /*          Cette fonction va permettre de vérifier les conditions pour faire rembourser un      */
    /*          client (que la réservation existe et qu'elle a été payée).                           */
    /*      -----------------------------------------------------------------------------------      */
    public void clientAnnuler(Reservation reserv)
    {
        if(listReserv.contains(reserv))
        {
            if(reserv.reservAnnuler())
            {
                this.rembourser();;   
            }
        }
    }
}