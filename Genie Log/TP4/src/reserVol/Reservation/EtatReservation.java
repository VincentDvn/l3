package reserVol.Reservation;

/*          Enumération des différents états de réservation possibles           */

enum EtatReservation
{
    EN_ATTENTE,
    ANNULEE,
    CONFIRMEE
}