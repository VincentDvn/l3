package reserVol.Reservation;

import java.util.ArrayList;

public class Passager extends Personne
{
    private String nom;
    private String email;
    private String telephone; 
    private String passeport;       //Correspond à son Numéro de passeport
    private int age;
    
    /*          On vérifie que son âge soit possible (0 < age < 120)            */

    public Passager(String name, String mail, String tel, String passport, int old)
    {
        super(name, mail, tel);
        if(old > 120 || old < 0)
        {
            throw new IllegalArgumentException("Age non correcte");
        }
        else
        {
            this.age = old;
        }     
        this.passeport = passport;
    }

}