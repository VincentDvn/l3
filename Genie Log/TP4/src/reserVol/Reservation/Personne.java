package reserVol.Reservation;

public abstract class Personne
{
    private String nom;
    private String email;
    private String telephone; 

    public Personne(String name, String mail, String tel)
    {
        this.nom = name;
        this.email = mail;
        this.telephone = tel;
    }
}