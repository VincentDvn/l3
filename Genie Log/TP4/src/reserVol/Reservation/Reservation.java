package reserVol.Reservation;

import reserVol.GestionVol.Vol;
import java.time.LocalDateTime;


public class Reservation
{

    private Client reservClient;
    private Passager reservPassager;
    private Vol reservVol;
    private LocalDateTime dateDepart;
    private EtatReservation reservEtat;

    /*          Pour réserver un vol, il faut que ce dernier soit ouvert au réservation,      */
    /*          on va donc vérifier cela.                                                     */
    public Reservation(Vol volAReserver, Passager passanger, Client customers)
    {
        if(volAReserver.ouvert())
        {
            this.reservVol = volAReserver;
            this.dateDepart = volAReserver.getDateDepart();
            this.reservPassager = passanger;
            this.reservClient = customers;
        }
        else
        {
            throw new IllegalArgumentException("Le vol que vous vouliez réserver est fermé");
        }
        this.reservEtat = EtatReservation.EN_ATTENTE;
        this.reservVol.newReserv(this);
    }

    /*      -----------------------------------------------------------------------------------      */
    /*          Cette fonction va renvoyer "true" si la confirmation nécessite au client             */
    /*          de payer. Sinon, renvoie false. Par ailleurs, elle va changer l'état de la           */
    /*          réservation en CONFIRMER lorsqu'il le faut (EN_ATTENTE).                             */
    /*          Cette fonction a un effet de bord et retourne un valeur. On fait cela pour           */
    /*          éviter que le client paye 2 fois suite à un bug quelconque.                          */
    /*      -----------------------------------------------------------------------------------      */
    public boolean reservConfirmer()
    {
        switch(this.reservEtat)
        {
            case EN_ATTENTE:
                this.reservEtat = EtatReservation.CONFIRMEE;
                return true;
                
            
            case CONFIRMEE:
                return false;
                            
            case ANNULEE:
                throw new IllegalArgumentException("La réservation est déjà annulé, vous ne pouvez pas la confirmer");
            
            default:
                return false;
        }
    }

    /*      -----------------------------------------------------------------------------------      */
    /*          Cette fonction va renvoyer "true" si la confirmation nécessite au client             */
    /*          d'être remboursé, sinon, renvoie false. Par ailleurs, elle va changer l'état         */
    /*          de la réservation en ANNULER lorsqu'il le faut (EN_ATTENTE || CONFIRMER).            */
    /*          Cette fonction a un effet de bord et retourne un valeur. On fait cela pour           */
    /*          éviter que le client soit remboursé 2 fois suite à un bug quelconque.                */
    /*      -----------------------------------------------------------------------------------      */
    public boolean reservAnnuler()
    {
        switch(this.reservEtat)
        {
            case CONFIRMEE:
                this.contacterClient();
                return true;
                
            case EN_ATTENTE:
                this.contacterClient();
                return false;
                
            case ANNULEE:
                return false;
            
            default:
                return false;
                
        }
    }


    /*          Cette fonction va appeler la fct contacter du client. Cette dernière lui enverra un   */
    /*          message lui avertissant que son vol (ou sa réservation) a été annulé.                 */
    public void contacterClient()
    {
        this.reservClient.contacter(this);
    }

    /*          Cette fonction va supprimer le vol de la réservation. Elle sera appelé par la classe    */
    /*          Vol lorsqu'on voudra annuler ce vol. On compte sur le Garbage Collector pour supprmier  */
    /*          l'objet réservation suite au déréférencement.                                           */
    public void suprVol()
    {
        this.reservVol = null;
    }
}
