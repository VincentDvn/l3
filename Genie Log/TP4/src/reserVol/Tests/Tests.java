package reserVol.Tests;

import reserVol.GestionVol.*;
import reserVol.Reservation.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Callable;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/*	--------------------------------------------------		*/
/*		A noter : ces Tests n'ont pas pour buts 		*/
/*		d'être exhaustif.					*/
/*	--------------------------------------------------		*/


public class Tests
{
    @Test
    public void testNewCompany()
    {
        Compagnie test = new Compagnie("AirFranceKLM");
        assertThat(test.toString(), equalTo("AirFranceKLM"));
    }

    @Test
    public void testNewVol()
    {
        Compagnie test = new Compagnie("AirFranceKLM");
        Trajet testTrajet = new Trajet(new Saut(new Etape(LocalDateTime.of(2020, 10, 23, 17, 36), new Aeroport(new Ville("Paris"))), new Etape(LocalDateTime.of(2020, 10, 23, 17, 37), new Aeroport(new Ville("New York")))));
        test.ajouterVol(testTrajet);

        assertThat(test.premierVol().volTrajet(), equalTo(testTrajet));
        
    }

    @Test
    public void testNewReservation()
    {
        Compagnie testCompagny = new Compagnie("AirFranceKLM");
        Trajet testTrajet = new Trajet(new Saut(new Etape(LocalDateTime.of(2020, 10, 23, 17, 36), new Aeroport(new Ville("Paris"))), new Etape(LocalDateTime.of(2020, 10, 23, 17, 37), new Aeroport(new Ville("New York")))));
        testCompagny.ajouterVol(testTrajet);
        Vol testVol = testCompagny.premierVol();
        testVol.ouvrir();

        Client testClient = new Client("Dupont", "dupont@gmail.com", "02 45 45 45 45", "RIB");
        testClient.reserver(testVol, new Passager("Dupond", "dupond@gmail.com", "02 46 46 46 46", "123456789", 42));
        
        assertThat(testVol.ouvert(), equalTo(true));
    }
    
    @Test
    public void testFermeture()
    {
        Compagnie testCompagny = new Compagnie("AirFranceKLM");
        Trajet testTrajet = new Trajet(new Saut(new Etape(LocalDateTime.of(2020, 10, 23, 17, 36), new Aeroport(new Ville("Paris"))), new Etape(LocalDateTime.of(2020, 10, 23, 17, 37), new Aeroport(new Ville("New York")))));
        testCompagny.ajouterVol(testTrajet);
        Vol testVol = testCompagny.premierVol();
        testVol.ouvrir();

        Client testClient = new Client("Dupont", "dupont@gmail.com", "02 45 45 45 45", "RIB");
        testClient.reserver(testVol, new Passager("Dupond", "dupond@gmail.com", "02 46 46 46 46", "123456789", 42));
        
        testVol.fermer();

        assertThat(testVol.ouvert(), equalTo(false));
    }


}
