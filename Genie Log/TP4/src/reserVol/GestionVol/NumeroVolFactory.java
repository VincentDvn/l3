package reserVol.GestionVol;

/*      -----------------------------------------------------------------------------      */
/*          Les classes Factories doivent être statique pour ne pas pouvoir                */
/*          les instancier. En java, pour rendre une classe static, on met son             */
/*          constructeur en privée.                                                        */
/*          La Factory n'a qu'une méthode qui crée un nouveau NumeroVol unique.            */
/*          le paramètre simpleInteger va permettre l'unicité de chaque NumeroVol.         */
/*          L'utilisation d'un Itérateur déguisé comme ici n'est pas des plus              */
/*          pertinents mais pour notre utilisation, cela suffira.                          */
/*      -----------------------------------------------------------------------------      */

public class NumeroVolFactory
{

    private static int simpleInteger = 0;

    private NumeroVolFactory()
    {

    }

    public static NumeroVol nouveauNumeroVol(String compagnie)
    {
        String str = "" + simpleInteger;
        simpleInteger ++;
        return (new NumeroVol(compagnie + str));
    }

}