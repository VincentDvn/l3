package reserVol.GestionVol;

import java.time.LocalDateTime;

public class Etape
{
    
    private Aeroport aeroportEtape;
    private LocalDateTime dateEtape;

    public Etape(LocalDateTime date, Aeroport airport)
    {
        this.aeroportEtape = airport;
        this.dateEtape = date;
    }

    public Aeroport getAeroport()
    {
        return this.aeroportEtape;
    }

    public LocalDateTime getDate()
    {
        return this.dateEtape;
    }
/*      On protège le set avec un protected. On veut que seul Saut.java y accède          */
    protected void setDate(LocalDateTime newDate)
    {
        this.dateEtape = newDate;
    }
    
}