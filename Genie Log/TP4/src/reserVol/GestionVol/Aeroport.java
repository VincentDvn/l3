package reserVol.GestionVol;

import java.util.ArrayList;

public class Aeroport
{

    private String nom;
    private ArrayList<Ville> villeDeservis = new ArrayList<>();

    /*          Pour être sur qu'un Aéroport possède au moins une ville,           */
    /*          on va demander une ville au constructeur.                          */
    public Aeroport(Ville city)         
    {
        villeDeservis.add(city);
    }

    public void ajouterVille(Ville city)
    {
        villeDeservis.add(city);
    }

    public String toString()
    {
        return this.nom;
    }
}