package reserVol.GestionVol;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Trajet
{
    private long duree;               

    private ArrayList<Saut> listSaut = new ArrayList<>();

    /*          Le constructeur appel un saut car l'objet Trajet doit          */
    /*          obligatoirement en contenir au moins un.                       */
    public Trajet(Saut jump)    
    {
        listSaut.add(jump);

        for(Saut sautIte : listSaut)
        {
            this.duree +=  sautIte.sautDuree();
        }

    }

    /*      -------------------------------------------------------------      */
    /*          Cette fonction va ajouter un saut au trajet. Elle doit         */
    /*          dabord vérifier que les aéroports concordent, le dernier       */
    /*          arrivé doit être égale au départ du saut que l'on rajoute      */
    /*      -------------------------------------------------------------      */
    public void ajouterSaut(Saut jump)
    {
        if(listSaut.get(listSaut.size()-1).getAeroArrivee().equals(jump.getAeroDepart()))
        {
            listSaut.add(jump);

            for(Saut sautIte : listSaut)
            {
                this.duree +=  sautIte.sautDuree();
            }
        }
        else
        {
            throw new IllegalArgumentException("Les Aéroports Départs et Arrivés ne concordent pas, les voyageurs ne peuvant pas se téléporter entre 2 aéroports !");
        }
    }
    /*          Fonction setters pour modifier les dates du trajet          */
    /*          (cette fonction sert dans le cas d'une réutilisation d'un   */
    /*          trajet pour un autre vol où il faut actualiser les dates)   */
    protected void setDate(LocalDateTime newDateDep, LocalDateTime newDateArriv)
    {
        this.listSaut.get(0).setDateDepart(newDateDep);
        this.listSaut.get(listSaut.size()-1).setDateArrivee(newDateArriv);
    }
    /*          Fonction Getters pour obtenir la date d'arrivée du trajet    */
    public LocalDateTime getDateArrivee()
    {
        return this.listSaut.get(listSaut.size()-1).getEtapeArrivee().getDate();
    }

    /*          Fonction Getters pour obtenir la date de départ du trajet    */
    public LocalDateTime getDateDepart()
    {
        return this.listSaut.get(0).getEtapeDepart().getDate();
    }

    /*          Fonction Getters pour obtenir la durée totale du trajet      */
    /*          Etant mise à jour à chaque ajout de saut, on n'a pas besoin  */
    /*          de la recalculer.                                            */
    public long trajetDuree()
    {
        return this.duree;
    }
}