package reserVol.GestionVol;

import java.util.ArrayList;

public class Compagnie
{
    private String nom;
    private ArrayList<Vol> listVol = new ArrayList<>(); 


    public Compagnie(String name)
    {
        this.nom = name;
    }

    public void ajouterVol(Trajet path) //Est public pour les tests. Elle devrait être privée sinon
    {
        listVol.add(new Vol(path, NumeroVolFactory.nouveauNumeroVol(this.nom)));    
    }

    /*          Cette fonction ne sert que dans les tests où il faut accéder                     */
    /*          premier vol de la liste. On ne peut pas y accéder directement car la liste       */
    /*          est privée. La rendre public est moins pertinente que cette fonction.            */
    public Vol premierVol()
    {
        return this.listVol.get(0);    
    }

    public String toString()
    {
        return this.nom;
    }

}
