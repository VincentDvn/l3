package reserVol.GestionVol;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Saut
{
    /*      --------------------------------------------------------------------------       */
    /*          Un saut contient 2 étapes (1 arrivée et 1 départ).                           */
    /*          Chaque étape a sa propre date et son propre aéroport.                        */
    /*                                                                                       */
    /*          On vérifie la conformité lors d'une création d'un nouveau saut.              */
    /*          Pour cela, on regarde les dates. Les aéroports ne nous intéresse pas.        */
    /*      --------------------------------------------------------------------------       */
    private Etape etapeDepart;
    private Etape etapeArrivee;

    public Saut(Etape depart, Etape arrive)
    {
        if(depart.getDate().isAfter(arrive.getDate()))
        {
            throw new IllegalArgumentException("Date d'arrivée avant date de Départ");
        }

        this.etapeDepart = depart;
        this.etapeArrivee = arrive;
    }

    /*          Cette fonction va retourner la durée du saut en l'a calculant grâce             */
    /*          au méthode de ChronoUnit.                                                       */
    public long sautDuree()
    {
        return ChronoUnit.MINUTES.between(this.etapeDepart.getDate(), this.etapeArrivee.getDate());
    }

    /*      -----------------------------------------------------------------------------       */
    /*          Ces fonctions sont des Setters et des Getters. Les setters sont protégés        */
    /*          avec protected pour être sur que seul les méthodes du package puisse les        */
    /*          appeler.                                                                        */
    /*      -----------------------------------------------------------------------------       */
    protected void setDate(LocalDateTime newDateArriv, LocalDateTime newDateDepar)
    {
        this.etapeArrivee.setDate(newDateArriv);
        this.etapeDepart.setDate(newDateDepar);
    } 

    protected void setDateDepart(LocalDateTime newDateDepar)
    {
        this.etapeDepart.setDate(newDateDepar);
    }

    protected void setDateArrivee(LocalDateTime newDateArriv)
    {
        this.etapeArrivee.setDate(newDateArriv);
    }

    public Aeroport getAeroDepart()
    {
        return this.etapeDepart.getAeroport();
    } 

    public Aeroport getAeroArrivee()
    {
        return this.etapeArrivee.getAeroport();
    } 

    public Etape getEtapeDepart()
    {
        return this.etapeDepart;
    }

    public Etape getEtapeArrivee()
    {
        return this.etapeArrivee;
    }
}
