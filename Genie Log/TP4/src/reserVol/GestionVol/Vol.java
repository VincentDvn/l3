package reserVol.GestionVol;

import reserVol.Reservation.Reservation;
import java.util.ArrayList;
import java.time.LocalDateTime;

public class Vol 
{
    private ArrayList<Reservation> listReserv = new ArrayList<>();
    private long duree;
    private Trajet trajetVol;
    private Boolean volOuvert;
    private NumeroVol ID;
    private LocalDateTime dateDepart;
    private LocalDateTime dateArrivee;

    /*      ---------------------------------------------------------------------------        */
    /*          On va utiliser plusieurs constructeurs pour palier les différentes             */
    /*          possibilités (Trajet déjà existant où il faudra actualiser les dates,          */
    /*          un nouveau trajet que l'on construira sur le tas ou un trajet déjà fait)       */
    /*      ---------------------------------------------------------------------------        */

    public Vol(Trajet path, NumeroVol id, LocalDateTime dateDep, LocalDateTime dateArriv)
    {
        this.dateDepart = dateDep;
        this.dateArrivee = dateArriv;
        this.trajetVol = path;  
        this.trajetVol.setDate(dateDep, dateArriv);   
        this.ID = id;   
        this.duree = trajetVol.trajetDuree();
    }

    public Vol(Trajet path, NumeroVol id)
    {
        this.trajetVol = path;
        this.ID = id;
        this.dateArrivee = path.getDateArrivee();
        this.dateDepart = path.getDateDepart();
        this.duree = trajetVol.trajetDuree();
    }

    public Vol(NumeroVol id, LocalDateTime dateDep, LocalDateTime dateArriv, Aeroport airportDep, Aeroport airportArriv)
    {
        this.ID = id;
        this.trajetVol = new Trajet(new Saut(new Etape(dateDep, airportDep), new Etape(dateArriv, airportArriv)));
        this.dateArrivee = dateArriv;
        this.dateDepart = dateDep;
        this.duree = trajetVol.trajetDuree();
    }

    /*          Fonction Getter pour savoir si le vol est Ouvert au réservation ou non.          */
    public boolean ouvert()
    {
        return this.volOuvert;
    }

    /*          Fonction Getters pour accéder au trajet du vol.                                  */
    public Trajet volTrajet()
    {
        return this.trajetVol;
    }

    /*          Fonction Getters pour accéder à la durée du vol.                                 */
    public long volDuree()
    {
        return this.duree;
    }

    /*          Fonction Getters pour accéder à la date du départ                                */
    public LocalDateTime getDateDepart()
    {
        return this.dateDepart;
    }

    /*          Fonction qui va ouvrir le vol aux réservations                                   */
    public void ouvrir()
    {
        this.volOuvert = true;
    }

    /*      -----------------------------------------------------------------------------        */
    /*          Fonction qui va ajouter une nouvelle réservation. Elle va être appelé            */
    /*          lors de la construction d'une nouvelle réservation. L'objectif étant d'assuré    */
    /*          la double cardinalité (La réservation connaît son vol et le vol connaît          */
    /*          toutes les réservations sur lui.                                                 */
    /*      -----------------------------------------------------------------------------        */

    public void newReserv(Reservation reservAdd)
    {
        listReserv.add(reservAdd);
    }

    /*      -----------------------------------------------------------------------------        */
    /*          Cette fonction va fermer les réservations, appeler une fonction permettant       */
    /*          de contacter les clients pour les rembourser/avertir et déréférencera les        */
    /*          réservations. On comptera sur le Garbage Collector de Java pour supprimer        */
    /*          proprement les objets réservations qui ont été déréférencé par cette fonction    */
    /*      -----------------------------------------------------------------------------        */

    public void fermer()
    {
        for(Reservation reserv : listReserv)
        {
            reserv.contacterClient();
            reserv.suprVol();
        }
        this.listReserv.clear();    //Vive le Garbage Collector de JAVA
        this.volOuvert = false;
    }
}