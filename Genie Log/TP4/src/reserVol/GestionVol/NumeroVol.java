package reserVol.GestionVol;

public class NumeroVol
{
    private final String numero;

    public NumeroVol(String num)
    {
        this.numero = num;
    }

    public String toString()
    {
        return this.numero;
    }

}